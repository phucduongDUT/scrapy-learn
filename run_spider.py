import scrapy
from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from learn_scrapy.spiders.quotes_spider import QuotesSpider
from scrapy.utils.project import get_project_settings

s = get_project_settings()

s.update({
    "LOG_ENABLED": "False"
})

# init the logger using setting
configure_logging(s)
runner = CrawlerRunner(s)

runner.crawl(QuotesSpider)
d = runner.join()
d.addBoth(lambda _: reactor.stop())

reactor.run()


# from scrapy.crawler import CrawlerProcess
# from scrapy.utils.project import get_project_settings
#
# process = CrawlerProcess(get_project_settings())
#
# # 'followall' is the name of one of the spiders of the project.
# process.crawl('quotes')
# process.start() # the script will block here until the crawling is finished