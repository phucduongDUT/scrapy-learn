# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html
import datetime

import scrapy
from scrapy.item import Item, Field
from scrapy.loader.processors import MapCompose, TakeFirst


def remove_quotes(text):
    return text.strip(u'\u201c'u'\u201d')


def convert_date(text):
    return text


def parse_location(text):
    return text[3:]


class QuoteItem(Item):
    # MapCompose enagle use multiple processing functions
    quote_content = Field(
        input_processor=MapCompose(remove_quotes),
        output_processor=TakeFirst()
    )
    author_name = Field(
        input_processor=MapCompose(str.strip),
        output_processor=TakeFirst()
    )
    tags = Field()
    date_of_birth = Field(
        # input_processor=MapCompose(convert_date),
        # output_processor=TakeFirst
    )
    birth_location = Field(
        input_processor=MapCompose(parse_location),
        output_processor=TakeFirst()
    )
