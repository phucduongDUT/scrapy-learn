import scrapy
from learn_scrapy.items import QuoteItem
from scrapy.loader import ItemLoader


class QuotesSpider(scrapy.Spider):
    name = 'quotes'

    start_urls = ["http://quotes.toscrape.com"]

    def parse(self, response):
        self.logger.info('hello Phuc')
        quotes = response.xpath("//div[@class='quote']")

        for quote in quotes:
            loader = ItemLoader(item=QuoteItem(), selector=quote)

            loader.add_css('author_name', '.author::text')
            loader.add_css('quote_content', '.text::text')
            loader.add_css('tags', '.tag::text')

            quote_item = loader.load_item()

            author_url = quote.xpath("./span[2]/a").attrib.get('href')
            self.logger.info("get author detail in link" + author_url)

            yield response.follow(author_url, callback=self.parse_author, meta={'quote_item': quote_item})

        next_page = response.xpath("//li[@class='next']/a").attrib.get('href')
        if next_page:
            yield response.follow(next_page, callback=self.parse)

    def parse_author(self, response):
        quote_item = response.meta['quote_item']

        loader = ItemLoader(item=quote_item, response=response)
        loader.add_css('author_name', '.author_title::text')
        loader.add_css('date_of_birth', '.author-born-date::text')
        loader.add_css('birth_location', '.author-born-location::text')

        yield loader.load_item()